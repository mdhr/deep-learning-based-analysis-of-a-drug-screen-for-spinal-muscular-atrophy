
# Spinal Muscular Atrophy Drug Screening Analysis with Deep Learning

## Overview
This repository contains the Python code associated with the master thesis focused on the deep-learning-based analysis of Spinal Muscular Atrophy (SMA) drug screening. This project focuses on a deep-learning-based analysis of a Spinal Muscular Atrophy drug screening using Risdiplam and Prednisolone. This thesis aims to develop a deep learning-based classification model to identify phenotype rescue patterns in a drug screening for Spinal Muscular Atrophy (SMA), in order to explore the potential of deep learning in optimizing the identification of potential drug candidates for the treatment of SMA. Additionally, this study integrates unsupervised machine learning techniques, particularly employing an autoencoder architecture, to complement the deep learning approach.

## Project Structure
The repository is structured as follows:

- `notebooks_Analysis_Stainings/`: Jupyter notebooks containing supervised learning approach.
- `notebooks_Autoencoders/`: Jupyter notebooks containing unsupervised learning approach.
- `notebooks_RGB_generators/`: Jupyter notebooks for RGB image generation.
- `README.md`: This file.

## Dependencies
The project requires the following libraries and tools:
- Python 3.x
- PyTorch 
- Pillow
- NumPy
- Pandas
- Scikit-learn
- Matplotlib
- Seaborn
- Captum
- Jupyter Notebook
- fastai
- os
## Computational Resources
In this study, the implementation and analysis of deep learning models for SMA drug screening were significantly enhanced by utilizing GPU resources provided by the Max Delbrück Center (MDC).
## Data
The data consists of images from the Spinal Muscular Atrophy (SMA) drug screening. 

### Data Preprocessing
Before training the models, the data needs to be preprocessed. This includes generating the RGB images that are used for the training tasks. The preprocessing scripts are located in the `notebooks_RGB_generator` folder for wild type, disease and rescue conditions.

### Supervised Learning 
To analyze the SMA drug screening data using a supervised deep learning approach, convolutional neural network (CNN) classifiers were trained. The classification models, including the rescue files analysis and the Explainable AI analysis are located in the `notebooks_analysis_stainings` folder for the three different staining sets.

### Unsupervised Learning
The project also integrates unsupervised machine learning techniques, specifically an autoencoder, to extract latent features and identify phenotype patterns. The developed autoencoder architecture is located in the `notebooks_autoencoder` folder, which includes the two different architecture implementations.

## Conclusion
This study leverages supervised learning and unsupervised learning techniques to analyze SMA drug screening data, aiming to optimize the identification of potential drug candidates for SMA treatment. The developed pipeline and methodologies contribute to a robust framework for future SMA drug screening analysis.


